#!/usr/bin/python3

# import required dependencies
import os   # importing os module so that code remains compaitable with different operating systems like linux and windows 
import os.path
from os.path import isfile
from Crypto import Random    # Importing Crypto so that we could use different cryptographic algorithms available in this module
from Crypto.Random import get_random_bytes
from Crypto.Cipher import PKCS1_OAEP    # is an asymmetric cipher based on RSA and the OAEP padding
from Crypto.PublicKey import RSA    # asymmetric cipher
from Crypto.Cipher import AES   # symmetric cipher
import time # This module provides various time-related functions

#creating class Encryptor
class Encryptor:
	def __init__(self, key):    # creating constructor for class Encryptor to whichwe passed the key as an argument
		self.key=key    # key will be the string which will be used to encrypt or decrypt our data

	def pad(self,s):    # creating a function for padding -> padding is the way to take data that may or may not be the multiple of block size for cipher and extend it
		return s+b"\0" * (AES.block_size - len(s) % AES.block_size)

	def encrypt(self, message, key, keySize = 256): # function to encrypt data which takes message, key, keysize as arguments
		message = self.pad(message)     # adding padding to our message
		iv = Random.new().read(AES.block_size)  # creating initialization vector -> random string of blocksize of cipher
		cipher = AES.new(key, AES.MODE_CBC, iv) # creating object for the cipher
		return iv + cipher.encrypt(message) # encrypting the message and append it to the initialization vector

	def encryptFile(self, fileName):    # creating function encryptfile which takes filename as argument
		with open(fileName, 'rb') as fo:    # opening file 
			plainText = fo.read()   # reading data from file
		enc = self.encrypt(plainText, self.key) # encrypting data
		with open(fileName + ".encrypted", 'wb') as fo: # creating and opening a new file with same_name but with extension ".ecnrypted"
			fo.write(enc)   # writing encrypted data to newly opened file
		os.remove(fileName) # removing/deleting the orignal file

if os.path.isfile("S_key"):		#if key exists, use it
	file = open('S_key', 'rb')
	key = file.read()
	file.close()
else:		#else generate a new key, write this key to a file for next use and use it for encryption
	key = os.urandom(32)
	file = open('S_key','wb')
	file.write(key) #the key is type bytes
	file.close()

enc = Encryptor(key)    #creating object of encryptor class
clear = lambda:os.system('cls')

fileToEncrypt = input("Enter filename to encrypt: ") #asking for filename to encrypt

if os.path.isfile(fileToEncrypt):   # checking if file exists in current directory
	enc.encryptFile(fileToEncrypt)  # if yes, encrypt it using encryptFile function of Encryptor class we created above
else:
	print("file doesn't exist in current directory")    # if file not found, exit
	exit()

pubKey = RSA.importKey(open("public.pem").read())  #importing available public key for encryption
session_key = key   # using generated key as session key
cipher_rsa = PKCS1_OAEP.new(pubKey) #PKCS1_OAEP is an asymmetric cipher based on RSA and the OAEP padding and we are creating its object here so that we could encrypt the session key in next step
enc_session_key = cipher_rsa.encrypt(session_key)   # encrypting session key [ the key we created using symmetric cipher ]

file = open("S_key.bin", 'wb') # creating and opening file key.key and adding ".enc" extension to it
file.write(enc_session_key) # writing encrypted session key to this file
os.remove('S_key')    # removing plaintext session key
file.close()    #closing file
