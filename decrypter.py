#!/usr/bin/python3
import os   # importing os module so that code remains compaitable with different operating systems like linux and windows 
import os.path
from os.path import isfile
# Importing Crypto so that we could use different cryptographic algorithms available in this module
from Crypto.Cipher import AES   # symmetric cipher
from Crypto.Cipher import PKCS1_OAEP    # is an asymmetric cipher based on RSA and the OAEP padding
from Crypto.PublicKey import RSA    # asymmetric cipher
import time # This module provides various time-related functions

# creating a Decryptor class
class Decryptor:
	def __init__(self, key):    # creating constructor for class Deccryptor to whichwe passed the key as an argument
		self.key=key     #key will be the string which will be used to encrypt or decrypt our data

	def pad(self,s):    # creating a function for padding -> padding is the way to take data that may or may not be the multiple of block size for cipher     and extend it
		return s+b"\0" * (AES.block_size -len(s) % AES.block_size)

	def decrypt(self, cipherText, key): # function to decrypt data which takes ciphertext, Key as arguments
		iv = cipherText[:AES.block_size]    # seperating out the initialization vector from the cipher text
		cipher = AES.new(key, AES.MODE_CBC, iv) # creating an object for the cipher
		plainText = cipher.decrypt(cipherText[AES.block_size:]) # decrypting the text
		return plainText.rstrip(b"\0")  # stripping the string for the additional padding which we added and return the orignal string

	def decryptFile(self, fileName):    #function to decrypt file which takes name of the file as an argument
		with open(fileName, 'rb') as fo: #open encrypted file
			cipherText = fo.read()  #read encrypted data
		dec  = self.decrypt(cipherText, self.key)   #decrypt encrypted data
		with open(fileName[:-10],'wb') as fo:   # removing last 10 characters from the filename that is making password.txtx.encrypted => password.txt
			fo.write(dec)   # writing decrypted data to file
		os.remove(fileName) # deleting encrypted file

if os.path.isfile("S_key.bin"): #checking if encrypted session key is present or not
    rsa_key = RSA.importKey(open("private.pem").read()) #importing private key
    cipher = PKCS1_OAEP.new(rsa_key)    # creating cipher object
    S_file = open('S_key.bin', "rb")    # opening encrypted session key
    sf = S_file.read()  # reading encrypted session key
    key = cipher.decrypt(sf)    # decryption encrypted session key
    file = open('S_key', "wb")  # writing decrypted session key to file
    file.write(key)
    file.close()
else:
    print("You don't seem to have a valid key")
    exit()


if os.path.isfile("S_key"): # checking if session key is present or not
	file = open('S_key', 'rb')  # opening file that contains session key
	key = file.read()   # reading session key
	file.close()
else:
	print("you don't seem to have a valid key")
	exit()

dec = Decryptor(key)    # creating object of Decryptor class

fileToDecrypt  = input("Enter File Name to decrypt: ") # asking user which file to decrypt
if os.path.isfile(fileToDecrypt):
	dec.decryptFile(fileToDecrypt)	# decrypting file with session key
else:
	print("file doesn't exist in current directory")
	exit()
